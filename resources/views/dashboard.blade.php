<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
     {{-- Header / Bannière --}}
    <div class="py-12">
        {{-- <div class="w-screen">
            <img class="w-full" src="{{ URL::asset('images/bg.png'); }}" alt="">
        </div> --}}
        <div class="max-w-screen-2xl mx-auto sm:px-6 lg:px-8">
            {{-- Listing des articles --}}
            <div class="grid grid-cols-3 gap-3 min-h-[20vh]">
                {{-- Dernier article posté --}}
                <div class="col-span-3 flex justify-center">
                    <x-last-article-posted />
                </div>
                {{-- Listing des articles avec pagination --}}
                <div>
                    @livewire('list-article-component')
                </div>
            </div>
            {{-- Sujet + pub + calendrier --}}
            <div class="grid grid-cols-3">
                {{-- forum --}}
                <div>
                    <x-last-topics />
                </div>
                {{-- Pub --}}
                <div>

                </div>
                {{-- Calendirer --}}
                <div>
                    @livewire('last-calendrier-event','')
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
