<footer class="bg-white">
    <div class="mx-auto max-w-7xl overflow-hidden py-12 px-4 sm:px-6 lg:px-8">
      <nav class="-mx-5 -my-2 flex flex-wrap justify-center" aria-label="Footer">
        <div class="px-5 py-2">
          <a href="/" class="text-base text-gray-500 hover:text-gray-900">Accueil</a>
        </div>
  
        <div class="px-5 py-2">
          <a href="{{ route("posts.index") }}" class="text-base text-gray-500 hover:text-gray-900">Blog</a>
        </div>
  
        <div class="px-5 py-2">
          <a href="{{ route("calendars.index") }}"class="text-base text-gray-500 hover:text-gray-900">Calendrier</a>
        </div>
  
        <div class="px-5 py-2">
          <a href="{{ route("channels.index") }}" class="text-base text-gray-500 hover:text-gray-900">Forum</a>
        </div>
  
        <div class="px-5 py-2">
          <a target="_blank" href="{{ URL::asset('documents/politique-de confidentialite.pdf'); }}"class="text-base text-gray-500 hover:text-gray-900">CGU</a>
        </div>
  
        <div class="px-5 py-2">
          <a target="_blank" href="{{ URL::asset('documents/reponses.pdf'); }}" class="text-base text-gray-500 hover:text-gray-900">Mention Légale</a>
        </div>
      </nav>
      <p class="mt-8 text-center text-base text-gray-400">&copy; 2022 TGCC. All rights reserved.</p>
    </div>
  </footer>
  