<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('lang.user.list') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div>
            @endif
                <div class="flex justify-end">
                    <a class="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-black bg-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" href="{{ route('users.create') }}">{{ __('lang.add.user') }}</a>
                </div>

                <div class="flex flex-col mt-8">
                    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                            <div class="bg-white shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                                <table class="min-w-full divide-y divide-gray-200 border-b">
                                    <thead class="bg-gray-50">
                                        <tr>
                                            <th scope="col"
                                                class="px-6 py-3 uppercase text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                {{ __('lang.user.name') }}
                                            </th>
                                            <th scope="col"
                                                class="px-6 py-3 uppercase text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                {{ __('lang.user.email') }}
                                            </th>
                                            <th scope="col"
                                                class="px-6 py-3 uppercase text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                {{ __('lang.user.role') }}
                                            </th>
                                            <th scope="col"
                                                class="px-6 py-3 uppercase text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                {{ __('lang.edit') }}
                                            </th>
                                            <th scope="col"
                                                class="px-6 py-3 uppercase text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                {{ __('lang.delete') }}
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody class="divide-y divide-gray-200">
                                    @foreach ($data as $key => $user)
                                        <tr class="bg-white">
                                            <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                                {{ $user->name }}
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                {{ $user->email }}
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                @if(!empty($user->getRoleNames()))
                                                    @foreach($user->getRoleNames() as $val)
                                                        <label class="flex-shrink-0 inline-block px-2 py-0.5 text-blue-800 text-xs font-medium bg-blue-100 rounded-full">{{ $val }}</label>
                                                    @endforeach
                                                @endif
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                @can('user-edit')
                                                    <a class="inline-flex items-center px-2.5 py-1.5 border-transparent text-xs font-medium rounded shadow-sm text-white bg-blue-500 cursor-pointer outline-none" href="{{ route('users.edit',$user->id) }}">{{ __('lang.edit') }}</a>
                                                @endcan
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                                @can('user-delete')
                                                    {!! Form::open(['method' => 'DELETE','route' => ['users.destroy',
                                                    $user->id]]) !!}
                                                    {!! Form::submit('Supprimer', ['class' => 'inline-flex items-center px-2.5 py-1.5 border-transparent text-xs font-medium rounded shadow-sm text-white bg-red-500 cursor-pointer outline-none']) !!}
                                                    {!! Form::close() !!}
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $data->render() }}
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</x-app-layout>
