<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('lang.add.permissions') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 bg-white">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Opps!</strong> Something went wrong, please check below errors.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <div class="py-6">
                {!! Form::open(array('route' => 'permissions.store','method'=>'POST' )) !!}
                <div class="grid grid-cols-6 gap-6 mt-6">
                    <div class="col-span-6 sm:col-span-3">
                        <label for="name"
                            class="block text-sm font-medium text-gray-700">{{ __('lang.permissions.name') }}</label>
                        {!! Form::text('name', null, array('placeholder' => 'Nom','class' => 'block p-2 mt-1 w-full
                        outline-none rounded-md focus:border-bleu-benin border-2 border-gray-700')) !!}
                    </div>

                </div>
                <div class="flex items-center justify-end mt-4">
                    <button type="submit" class="inline-flex items-center px-4 py-2 border border-transparent text-base font-medium rounded-md shadow-sm text-white  bg-bleu-benin hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        {{ __('lang.send') }}
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</x-app-layout>

