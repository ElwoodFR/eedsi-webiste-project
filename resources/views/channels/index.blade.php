<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Forum') }}
        </h2>
        @if(Session::has('success'))
            <div>
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
    </x-slot>
    <div class="py-12">

      <div class="max-w-7xl mx-auto py-6 mt-6 px-4 sm:px-6 lg:px-8 bg-white rounded">
          <div class="flex">
              @can('channel-edit')
                  <div class="mr-4">
                      <a class='inline-flex items-center rounded-md border border-transparent bg-indigo-100 px-3 py-2 text-sm font-medium leading-4 text-indigo-700 hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2'
                          href="{{ route('channels.create') }}">Nouvelle catégorie</a>
                  </div>
              @endcan
              @can('discussion-create')
                  <div class="">
                      <a class='inline-flex items-center rounded-md border border-transparent bg-indigo-100 px-3 py-2 text-sm font-medium leading-4 text-indigo-700 hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2'
                          href="{{ route('discussions.create') }}">Ajouter un nouveau topic</a>
                  </div>
              @endcan
  
          </div>
          <div class="grid grid-cols-3 gap-4">
              <div class="py-4">
                  <div class="sm:flex sm:items-center">
                      <div class="sm:flex-auto">
                          <h1 class="text-xl font-semibold text-gray-900">{{ __('Catégories') }}
                          </h1>
                      </div>
                  </div>
                  <div class="mt-3 flex flex-col">
                      <div class="-my-2 -mx-4 overflow-x-auto sm:-mx-6 lg:-mx-8">
                          <div class="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
                              <div class="overflow-hidden shadow ring-1 ring-black ring-opacity-5 md:rounded-lg">
                                  <table class="min-w-full divide-y divide-gray-300">
                                      <thead class="bg-gray-50">
                                          <tr>
                                              <th scope="col"
                                                  class="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6">
                                                  Catégorie</th>
                                              @can('channel-edit')
                                                  <th scope="col" class="relative py-3.5 pl-3 pr-4 sm:pr-6">
                                                      <span class="sr-only">Edit</span>
                                                  </th>
                                              @endcan
                                              @can('channel-destroy')
                                                  <th scope="col" class="relative py-3.5 pl-3 pr-4 sm:pr-6">
                                                      <span class="sr-only">Supprimer</span>
                                                  </th>
                                              @endcan
                                          </tr>
                                      </thead>
                                      <tbody class="divide-y divide-gray-200 bg-white">
                                          @foreach($channels as $channel)
                                              <tr>
                                                  <td
                                                      class="whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-6">
                                                      <a
                                                          href="{{ route("channels.show",[$channel->id]) }}">
                                                          <h5
                                                              class="mb-2 text-base font-bold tracking-tight text-gray-900 dark:text-white">
                                                              {{ $channel->title }}</h5>
                                                      </a>
                                                  </td>
                                                  @can('channel-edit')
                                                      <td
                                                          class="relative whitespace-nowrap py-4 pl-3 pr-4 text-right text-sm font-medium sm:pr-6">
                                                          <a href="{{ route('channels.edit',[$channel->id]) }}"
                                                              class="text-indigo-600 hover:text-indigo-900">Edit</a>
                                                      </td>
                                                  @endcan
                                                  @can('channel-destroy')
                                                      <td
                                                          class="relative whitespace-nowrap py-4 pl-3 pr-4 text-right text-sm font-medium sm:pr-6">
                                                          <form
                                                              action="{{ route('channels.destroy',[$channel->id]) }}"
                                                              method='post'>
                                                              @csrf
                                                              @method('delete')
                                                              <button class="">Supprimer</button>
                                                          </form>
                                                      </td>
                                                  @endcan
                                              </tr>
                                          @endforeach
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
  
              <div class="py-4 col-span-2 mx-4">
                  <x-last-topics />
              </div>
          </div>
      </div>

    </div>
</x-app-layout>
