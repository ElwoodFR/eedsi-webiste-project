<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Forum - Nouvelle catégorie') }}
        </h2>
    </x-slot>
    <div class="max-w-7xl mx-auto py-6 mt-6 px-4 sm:px-6 lg:px-8 bg-white rounded">
        <form action="{{route('channels.store')}}" method="POST" class="w-full sm:px-6 lg:px-0 lg:col-span-9 mx-auto relative pb-20 space-y-6"> 
            @csrf
            <div class="grid sm:grid-cols-2 sm:gap-4 sm:items-start sm:pt-5">
                <label for="title" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">{{__('Nom de la catégorie')}}</label>
                <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input type="text" class="max-w-lg block focus:ring-indigo-500 focus:border-indigo-500 w-full shadow-sm sm:max-w-xs sm:text-sm border-gray-300 rounded-md @error('title') border border-red-500 @enderror" name="title" />
                    @error('title') <span class="text-red-500">{{ $message }}</span>@enderror
                </div>
                <div>
                    <a href="{{ route('channels.index')}}"
                     class="bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">{{__('Cancel')}}</a>

                    <button type="submit"
                    class="ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">{{__('Save')}}</button>
                </div>
            </div>
        </form>
    </div>
</x-app-layout>
