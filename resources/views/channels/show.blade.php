<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
        
    </x-slot>
    <div>
        <div  class="max-w-7xl mx-auto py-6 mt-6 px-4 sm:px-6 lg:px-8 bg-white rounded">
            <h2 class="text-2xl ">{{$channel->title}}</h2>
            @can('discussion-create')
            <div class="mt-2">
                <a class="bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm font-medium text-gray-700 hover:bg-gray-50"
                href="{{route('discussions.create')}}">Ajouter un nouveau topic</a>
            </div>
            @endcan
            <div class="pt-5">
                @livewire('discussions-list')
            </div>
        </div>
    </div>    
</x-app-layout>
