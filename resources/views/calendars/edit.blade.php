<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Calendrier') }}
        </h2>
    </x-slot>

    <div class="py-12">
            @if($errors->any())
            <div class="text-red-500">
                <b>Veuillez corriger la ou les erreurs suivantes :</b>
                <ul>
                    @foreach($errors->all() as $error)
                        <li class="list-disc ml-8">{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{ route('calendars.update', $calendar->id) }}" method="POST"
            class="w-full sm:px-6 lg:px-0 lg:col-span-9 mx-auto relative pb-20 space-y-6" enctype="multipart/form-data">
            @csrf
            @method('PUT')

            {{-- ARTICLE MANAGEMENT --}}
            <div class="shadow sm:rounded-md sm:overflow-hidden">
                <div class="bg-white py-6 px-4 space-y-6 sm:p-6">
                    <div>
                        <h3 class="leading-6 font-medium text-gray-900 text-2xl tracking-wider uppercase"
                            id="status">
                            Information de l'évènement
                        </h3>
                    </div>


                    <div class="container mx-auto">
                        <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2">
                            <div class="justify-center">
                                <div class="space-y-6 sm:space-y-5">

                                    <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:pt-5">
                                        <label for="titre" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">Titre</label>
                                        <div class="mt-1 sm:mt-0 sm:col-span-2">
                                            <input type="text" value="{{ $calendar->titre }}" class="max-w-lg block focus:ring-indigo-500 focus:border-indigo-500 w-full shadow-sm sm:max-w-xs sm:text-sm border-gray-300 rounded-md @error('titre') border border-red-500 @enderror" name="titre" />
                                            @error('titre') <span class="text-red-500">{{ $message }}</span>@enderror
                                        </div>
                                    </div>

                                    <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:pt-5">
                                        <label for="description" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">Description</label>
                                        <div class="mt-1 sm:mt-0 sm:col-span-2">
                                            <input type="text" value="{{ $calendar->description }}"  class="max-w-lg block focus:ring-indigo-500 focus:border-indigo-500 w-full shadow-sm sm:max-w-xs sm:text-sm border-gray-300 rounded-md @error('description') border border-red-500 @enderror" name="description" />
                                            @error('description') <span class="text-red-500">{{ $message }}</span>@enderror
                                        </div>
                                    </div>

                                    <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:pt-5">
                                        <label for="date_debut" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">Date de début</label>
                                        <div class="mt-1 sm:mt-0 sm:col-span-2">
                                            <input type="datetime-local" value="{{ $calendar->date_debut }}"  class="max-w-lg block focus:ring-indigo-500 focus:border-indigo-500 w-full shadow-sm sm:max-w-xs sm:text-sm border-gray-300 rounded-md @error('date_debut') border border-red-500 @enderror" name="date_debut" />
                                            @error('date_debut') <span class="text-red-500">{{ $message }}</span>@enderror
                                        </div>
                                    </div>

                                    <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:pt-5">
                                        <label for="date_fin" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">Date de fin (optionnel)</label>
                                        <div class="mt-1 sm:mt-0 sm:col-span-2">
                                            <input type="datetime-local" value="{{ $calendar->date_fin }}" class="max-w-lg block focus:ring-indigo-500 focus:border-indigo-500 w-full shadow-sm sm:max-w-xs sm:text-sm border-gray-300 rounded-md @error('date_fin') border border-red-500 @enderror" name="date_fin" />
                                            @error('date_fin') <span class="text-red-500">{{ $message }}</span>@enderror
                                        </div>
                                    </div>

                                    <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:pt-5">
                                        <label for="image" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">Image</label>
                                        <div class="mt-1 sm:mt-0 sm:col-span-2">
                                            <input type="file" placeholder="image" value="{{ $calendar->image }}"" class="max-w-lg block focus:ring-indigo-500 focus:border-indigo-500 w-full shadow-sm sm:max-w-xs sm:text-sm border-gray-300 rounded-md @error('image') border border-red-500 @enderror" name="image" />
                                            @error('image') <span class="text-red-500">{{ $message }}</span>@enderror
                                            <img src="/images/{{ $calendar->image }}" width="300px" class="my-4">
                                        </div>
                                    </div>

                                    <div class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:pt-5">
                                        <label for="sponsor" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">Sponsor</label>
                                        <div class="mt-1 sm:mt-0 sm:col-span-2">
                                            <input type="text" value="{{ $calendar->sponsor }}" class="max-w-lg block focus:ring-indigo-500 focus:border-indigo-500 w-full shadow-sm sm:max-w-xs sm:text-sm border-gray-300 rounded-md @error('sponsor') border border-red-500 @enderror" name="sponsor" />
                                            @error('sponsor') <span class="text-red-500">{{ $message }}</span>@enderror
                                        </div>
                                    </div>

                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="fixed bg-gray-200 bottom-0 inset-x-0 py-4 px-8 flex items-center justify-end transition duration-300" id="form-footer">
            <button type="button"
                class="bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Cancel</button>

            <button type="submit"
                class="ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Save</button>
        </div>
         
    </form>
    </div>
    </div>

</x-app-layout>
