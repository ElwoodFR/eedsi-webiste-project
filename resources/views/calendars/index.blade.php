<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Calendrier') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @can('event-create')
            <a href="{{ route('calendars.create') }}"
                class="inline-flex items-center px-4 py-2 my-3 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                Ajouter un évènement
            </a>
            @endcan

    @if (session('success'))
    <div class="bg-blue-500 p-4 mb-4">
        {{ session('success') }}
    </div>
    @endif
    <div class="overflow-hidden z-0">
        <div class="grid grid-cols-1 gap-6 sm:grid-cols-1 md:grid-cols-1 lg:grid-cols-2">
            @foreach($calendars as $calendar)
            <div class="border border-gray-200 bg-white rounded-xl hover:bg-gray-50 z-30">

                {{-- <!-- Badge -->
                @if($post->published_at !== null)
                <p class="bg-green-500 w-fit px-4 py-1 text-sm font-bold text-white rounded-tl-lg rounded-br-xl">
                    Publié
                </p>
                @else
                <p class="bg-red-500 w-fit px-4 py-1 text-sm font-bold text-white rounded-tl-lg rounded-br-xl">
                    Non publié
                </p>
                @endif --}}

                </p>

                <div class="grid grid-cols-6 p-5 gap-y-2">

                    <!-- Profile Picture -->
                    <div class="w-full h-full">
                        <img src="/images/{{ $calendar->image }}" alt="tailwind logo" class="rounded-xl" />
                    </div>

                    <!-- Description -->
                    <div class="col-span-5 md:col-span-4 ml-4">

                        <p class="text-gray-600 text-xl font-bold"> {{ $calendar->titre }}</p>

                        <div class="flex items-center">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-2 text-gray-400" fill="none"
                                viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                            </svg>
                            <p class="text-gray-400">
                                {{ $calendar->date_debut }}
                            </p>
                        </div>

                        <div class="flex items-center">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-2 text-gray-400" fill="none"
                                viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                            </svg>
                            <p class="text-gray-400">
                                {{ $calendar->date_fin }}
                            </p>
                        </div>
                    
                        @if($calendar->sponsor !== null)
                        <div class="flex items-center mt-4">
                            <a href="{{ $calendar->sponsor }}" class="text-sky-600">
                                Sponsor : {{ $calendar->sponsor }}
                            </a>
                        </div>
                        @endif
                        <br>
                        <p>{{ $calendar->description }}</p>

                    </div>
                    @can('event-edit')
                    <div class="flex col-start-2 ml-4 md:col-start-auto md:ml-0 md:justify-end z-50">
                        <div class="flex justify-end">
                            <div x-data="{ dropdownOpen: false }" class="absolute z-50">
                                <button @click="dropdownOpen = !dropdownOpen"
                                    class="relative z-10 block rounded-md bg-blue-100 p-2 focus:outline-none">
                                    <svg viewBox="0 0 24 24" width="16" height="16" stroke="currentColor"
                                        stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round"
                                        class="css-i6dzq1">
                                        <circle cx="12" cy="12" r="3"></circle>
                                        <path
                                            d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z">
                                        </path>
                                    </svg>
                                </button>

                                <div x-show="dropdownOpen" @click="dropdownOpen = false"
                                    class="fixed inset-0 h-full w-full z-10"></div>

                                <div x-show="dropdownOpen"
                                    class="absolute right-0 mt-2 py-2 w-48 bg-white rounded-md shadow-xl z-20">

                                    @can('event-edit')
                                    <a href="{{ route('calendars.edit', $calendar->id) }}"
                                        class="block px-4 py-2 text-sm capitalize text-gray-700 hover:bg-blue-500 hover:text-white">
                                        Editer
                                    </a>
                                    @endcan
                                    @can('event-delete')
                                    <form
                                        class="block px-4 py-2 text-sm capitalize text-gray-700 hover:bg-blue-500 hover:text-white"
                                        action="{{ route('calendars.destroy', $calendar->id) }}" method="POST">
                                        @csrf
                                        @method('delete')

                                        <button class="" type="submit">
                                            <i class="fa-solid fa-trash text-red-500"></i>
                                            Supprimer
                                        </button>
                                    </form>
                                    @endcan
                                </div>
                            </div>
                        </div>
                    </div>
                    @endcan
                </div>

            </div>
            @endforeach
        </div>
    </div>
    </div>
    </div>

</x-app-layout>
