<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
        <div>
            <p>{{__('Catégorie:')}} {{$discussion->channel->title}}</p>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="border rounded-xl bg-white p-2 my-2">
                        <h1 class="text-3xl">{{$discussion->title}}</h1>
                        <hr>
                        <div class="max-h-[35rem] overflow-y-scroll  my-2 p-2">
                            {!! $discussion->content !!}
                        </div>
                        <hr>
                        <h2 class="text lg">{{$discussion->user->name}} | {{ $discussion->created_at->format("d-m-Y h\hi")}}</h2>
                    </div>
                    <div class="my-2 bg-white p-2 rounded-xl">
                        <h3 class="text-2xl">{{__('Réponses')}}</h3>
                        <div>
                            @livewire('replies-list', ['discussion' => $discussion->id])
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
</x-app-layout>
