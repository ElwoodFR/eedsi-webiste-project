<div class="w-full border border-gray-200 rounded-sm">
    <div class="grid grid-cols-3">
        <div class="col-span-1 border-2 rounded-md">
            <img src="/images/{{$post->image}}" class="w-fit" alt="Cover {{$post->title}}">
        </div>
        <div class="col-span-2 prose max-h-[40vh] h-full overflow-hidden w-full ml-3">
            <h3>{{$post->name}}</h3>
            {!! $post->content !!}
        </div>
        <div class="col-span-1">
           <p>{{__('Publié le:')}} {{$post->published_at->format('d-m-Y')}}</p>
        </div>
        <div>
            <p>{{__('Catégorie:')}} {{$post->category->name}}</p>
        </div>
    </div>
</div>