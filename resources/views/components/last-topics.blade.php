
    <div class="sm:flex sm:items-center">
      <div class="sm:flex-auto">
        <h1 class="text-xl font-semibold text-gray-900">Derniers topics</h1>
      </div>
    </div>
    <div class="mt-3 flex flex-col">
      <div class="-my-2 -mx-4 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
          <div class="overflow-hidden shadow ring-1 ring-black ring-opacity-5 md:rounded-lg">
            <table class="min-w-full divide-y divide-gray-300">
              <thead class="bg-gray-50">
                <tr>
                  <th scope="col" class="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6">Topic</th>
                  <th scope="col" class="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6">Réponses</th>
                  <th scope="col" class="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6">Auteur</th>
                  <th scope="col" class="sr-only">Action</th>
                  @can('channel-edit')
                  <th scope="col" class="relative py-3.5 pl-3 pr-4 sm:pr-6">
                    <span class="sr-only">Edit</span>
                  </th>
                  @endcan
                  @can('channel-destroy')
                  <th scope="col" class="relative py-3.5 pl-3 pr-4 sm:pr-6">
                    <span class="sr-only">Supprimer</span>
                  </th>
                  @endcan
                </tr>
              </thead>
              <tbody class="divide-y divide-gray-200 bg-white">
                @foreach($discussions as $discussion)
                <tr>
                  <td class="whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-6">
                    <a href="{{route('discussion.show',$discussion->id)}}">
                        <h5 class="mb-2 text-base font-bold tracking-tight text-gray-900 dark:text-white">{{$discussion->title}}</h5>
                    </a>
                  </td>
                  <td class="whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-6">
    
                        <h5 class="mb-2 text-base font-bold tracking-tight text-gray-900 dark:text-white">{{$discussion->replies->count()}}</h5>
                  </td>
                  <td class="whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-6">
                        <h5 class="mb-2 text-base font-bold tracking-tight text-gray-900 dark:text-white">{{$discussion->user->name}}</h5>
                  </td>
                  @can('channel-destroy')
                  <td class="whitespace-nowrap py-4 pl-3 pr-4 text-right text-sm font-medium sm:pr-6">
                    <a href="#" class="text-indigo-600 hover:text-indigo-900">Supprimer</a>
                  </td>
                  @endcan
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>