<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Liste des rôles') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div>
            @endif
            @can('role-create')
                    <div class="flex justify-end">
                        <a class="inline-flex items-center mb-4 px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-black bg-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                            href="{{ route('roles.create') }}"
                        >
                            Ajouter un rôle
                        </a>
                    </div>
            @endcan

            <div class="flex flex-col">
                <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                        <div class="bg-white shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                            <table class="min-w-full divide-y divide-gray-200 border-b">
                                <thead class="bg-gray-50">
                                    <tr>
                                        <th scope="col"
                                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            ID
                                        </th>
                                        <th scope="col"
                                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            RÔLE
                                        </th>
                                        <th scope="col"
                                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            DETAIL
                                        </th>
                                        <th scope="col"
                                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            EDITER
                                        </th>
                                        <th scope="col"
                                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            SUPPRIMER
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="divide-y divide-gray-200">
                                    @foreach ($roles as $key => $role)
                                    <tr>
                                        <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                            {{ $role->id }}
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                            <strong>{{ $role->name }}</strong>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                            <a class="inline-flex items-center px-2.5 py-1.5 border-transparent text-xs font-medium rounded shadow-sm text-white bg-green-500 cursor-pointer outline-none" href="{{ route('roles.show',$role->id) }}">Détail</a>
                                        </td>
                                        @can('role-edit')
                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                            <a class="inline-flex items-center px-2.5 py-1.5 border-transparent text-xs font-medium rounded shadow-sm text-white bg-blue-500 cursor-pointer outline-none" href="{{ route('roles.edit',$role->id) }}">Editer</a>
                                        </td>
                                        @endcan
                                        @can('role-delete')
                                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                            {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy',
                                            $role->id],'style'=>'display:inline']) !!}
                                            {!! Form::submit('Supprimer', ['class' => 'inline-flex items-center px-2.5 py-1.5 border-transparent text-xs font-medium rounded shadow-sm text-white bg-red-500 cursor-pointer outline-none']) !!}
                                            {!! Form::close() !!}
                                        </td>
                                        @endcan
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="my-4 px-4">
                                {{ $roles->appends($_GET)->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
