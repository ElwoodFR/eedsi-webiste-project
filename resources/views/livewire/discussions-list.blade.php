<div>
    {{-- The best athlete wants his opponent at his best. --}}
    @foreach($discussions as $discussion)
        <div class="border rounded-sm shadow-md p-5 my-5">
            <h4 class="text-xl">{{$discussion->title}}</h4>
            <div class="grid grid-cols-3 gap-2 mt-4 py-2">
                <div>
                    <a 
                        class="bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm font-medium text-gray-700 hover:bg-gray-50"
                        href="{{route('discussion.show',$discussion->id)}}">
                        {{__('Acceder')}}
                    </a>
                </div>

                <div>
                    <p>{{__("Réponses:")}} {{$discussion->replies->count()}}</p>
                </div>

                <div>
                   <p>{{__('Auteur:')}} {{$discussion->user->name}}</p>
                </div>

            </div>
        </div>
    @endforeach
    {{ $discussions->links() }}
</div>
