<div>
    @if($calendar != null)
        <div class="border border-gray-200 bg-white rounded-xl hover:bg-gray-50 z-30">
            <!-- Profile Picture -->
            <div class="w-full h-full">
                <img src="/images/{{ $calendar->image }}" alt="{{ $calendar->titre }}" class="rounded-t-xl" />
            </div>
            <div class="p-5">
                <!-- Description -->
                <div class="ml-4">
                    <p class="text-gray-600 text-xl font-bold"> {{ $calendar->titre }}</p>
                    <div class="flex items-center">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-2 text-gray-400" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                        </svg>
                        <p class="text-gray-400">
                            {{ $calendar->date_debut }}
                        </p>
                    </div>

                    <div class="flex items-center">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-2 text-gray-400" fill="none"
                            viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                        </svg>
                        <p class="text-gray-400">
                            {{ $calendar->date_fin }}
                        </p>
                    </div>

                </div>
            </div>
        </div>
    @endif
</div>