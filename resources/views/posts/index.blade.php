<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Liste des articles') }}
        </h2>
    </x-slot>



    <div class="py-12">
        <div class="mx-auto sm:px-6 lg:px-8">
            @can('post-create')
            <div class="flex justify-end">
                <a class="inline-flex items-center mb-4 px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-black bg-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                    href="{{ route('posts.create') }}">
                    Ajouter un article
                </a>
            </div>
            @endcan

            <div class="relative px-4 pt-4 pb-20 sm:px-6 lg:px-8 lg:pt-4 lg:pb-28">
                <div class="absolute inset-0">
                  <div class="h-1/3 sm:h-2/3"></div>
                </div>
                <div class="relative mx-auto">
                  <div class="text-center">
                    <h2 class="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">Blog</h2>
                  </div>
                  <div class="mx-auto mt-12 grid max-w-lg gap-5 lg:max-w-none lg:grid-cols-3">
                    @foreach($posts as $post)
                    
                    <div class="flex flex-col overflow-hidden rounded-lg shadow-lg">
                          <div class="flex-shrink-0">
                            <img class="h-48 w-full object-cover" src="/images/{{ $post->image }}" alt="Couverture de l'article {{$post->title}}">
                          </div>
                          <div class="flex flex-1 flex-col justify-between bg-white p-6">
                            @can('post-edit')
                            <div class="flex col-start-2 ml-4 md:col-start-auto md:ml-0 md:justify-end">
                                <div class="flex justify-end">
                                    <div x-data="{ dropdownOpen: false }" class="relative">
                                        <button @click="dropdownOpen = !dropdownOpen"
                                            class="relative z-10 block rounded-md bg-blue-100 p-2 focus:outline-none">
                                            <svg viewBox="0 0 24 24" width="16" height="16" stroke="currentColor"
                                                stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round"
                                                class="css-i6dzq1">
                                                <circle cx="12" cy="12" r="3"></circle>
                                                <path
                                                    d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z">
                                                </path>
                                            </svg>
                                        </button>
        
    
                                        <div x-show="dropdownOpen" @click="dropdownOpen = false"
                                            class="fixed inset-0 h-full w-full z-10">
                                        </div>
        
                                        <div x-show="dropdownOpen"
                                            class="absolute right-0 mt-2 py-2 w-48 bg-white rounded-md shadow-xl z-50">
                                            <a href="{{ route('posts.edit', $post->id) }}"
                                                class="block px-4 py-2 text-sm capitalize text-gray-700 hover:bg-blue-500 hover:text-white">
                                                Editer
                                            </a>
                                            @can('post-delete')
                                            <form
                                                class="block px-4 py-2 text-sm capitalize text-gray-700 hover:bg-blue-500 hover:text-white"
                                                action="{{ route('posts.destroy', $post->id) }}" method="POST">
                                                @csrf
                                                @method('delete')
        
                                                <button class="" type="submit">
                                                    <i class="fa-solid fa-trash text-red-500"></i>
                                                    Supprimer
                                                </button>
                                            </form>
                                            <a href="#"
                                                class="block px-4 py-2 text-sm capitalize text-gray-700 hover:bg-blue-500 hover:text-white">
                                                Dépublier
                                            </a>
                                            @endcan
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endcan
                            <div class="flex-1">
                              <p class="text-sm font-medium text-indigo-600">
                                <a href="{{route('posts.show',[$post->id])}}" class="hover:underline">{{ $post->category->name }}</a>
                              </p>
                              <a href="{{route('posts.show',[$post->id])}}" class="mt-2 block">
                                <p class="text-xl font-semibold text-gray-900">{{ $post->name }}</p>
                                <p class="mt-3 text-base text-gray-500">{!! Illuminate\Support\Str::limit($post->content, 60) !!}</p>
                              </a>
                            </div>
                            <div class="mt-6 flex items-center">
                              <div class="">
                                <div class="flex space-x-1 text-sm text-gray-500">
                                  <time datetime="2020-03-16">
                                    @if($post->published_at !== null)
                                    <div class="flex items-center">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-2 text-gray-400" fill="none"
                                            viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                                        </svg>
                                        <p class="text-gray-400">
                                            {{ $post->published_at->format('d-m-Y') }}
                                        </p>
                                    </div>
                                    @endif
                                  </time>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    
                    @endforeach
                  </div>
                </div>
            </div>
            {{-- <div class="overflow-hidden">
                <div class="grid grid-cols-1 gap-6 sm:grid-cols-1 md:grid-cols-1 lg:grid-cols-2">
                    @foreach($posts as $post)
                    <div class="border border-gray-200 shadow-xl rounded-xl hover:bg-gray-50">
        
                        <div class="grid grid-cols-6 p-5 gap-y-2">
        
                            <!-- Profile Picture -->
                            <div class="w-full h-full">
                                <img src="/images/{{ $post->image }}" alt="Couverture de l'article {{$post->title}}" class="rounded-xl" />
                            </div>
        
                            <!-- Description -->
                            <div class="col-span-5 md:col-span-4 ml-4">
        
                                <p class="text-gray-600 text-xl font-bold"> {{ $post->name }}</p>
        
                                <p class="text-gray-400"> Catégorie : {{ $post->category->name }} </p>
                                <div class="text-gray-400"> Filtre : 
                                    @forelse ($post->tags as $tag)
                                        <span>{{$tag->name}}</span>
                                    @empty
                                        
                                    @endforelse
                                </div>
        
                                @if($post->published_at !== null)
                                <div class="flex items-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-2 text-gray-400" fill="none"
                                        viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                                    </svg>
                                    <p class="text-gray-400">
                                        {{ $post->published_at->format('d-m-Y') }}
                                    </p>
                                </div>
                                @endif
        
                            </div>
        
                            <!-- Price -->
                            @can('post-edit')
                            <div class="flex col-start-2 ml-4 md:col-start-auto md:ml-0 md:justify-end">
                                <div class="flex justify-end">
                                    <div x-data="{ dropdownOpen: false }" class="relative">
                                        <button @click="dropdownOpen = !dropdownOpen"
                                            class="relative z-10 block rounded-md bg-blue-100 p-2 focus:outline-none">
                                            <svg viewBox="0 0 24 24" width="16" height="16" stroke="currentColor"
                                                stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round"
                                                class="css-i6dzq1">
                                                <circle cx="12" cy="12" r="3"></circle>
                                                <path
                                                    d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z">
                                                </path>
                                            </svg>
                                        </button>
        

                                        <div x-show="dropdownOpen" @click="dropdownOpen = false"
                                            class="fixed inset-0 h-full w-full z-10">
                                        </div>
        
                                        <div x-show="dropdownOpen"
                                            class="absolute right-0 mt-2 py-2 w-48 bg-white rounded-md shadow-xl z-20">
                                            <a href="{{ route('posts.edit', $post->id) }}"
                                                class="block px-4 py-2 text-sm capitalize text-gray-700 hover:bg-blue-500 hover:text-white">
                                                Editer
                                            </a>
                                            @can('post-delete')
                                            <form
                                                class="block px-4 py-2 text-sm capitalize text-gray-700 hover:bg-blue-500 hover:text-white"
                                                action="{{ route('posts.destroy', $post->id) }}" method="POST">
                                                @csrf
                                                @method('delete')
        
                                                <button class="" type="submit">
                                                    <i class="fa-solid fa-trash text-red-500"></i>
                                                    Supprimer
                                                </button>
                                            </form>
                                            <a href="#"
                                                class="block px-4 py-2 text-sm capitalize text-gray-700 hover:bg-blue-500 hover:text-white">
                                                Dépublier
                                            </a>
                                            @endcan
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endcan
        
                        </div>
        
                    </div>
                    @endforeach
                </div>
            </div> --}}
        </div>
    </div>
</x-app-layout>