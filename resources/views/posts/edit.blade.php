<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edition article') }} - {{$post->name}}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">


            <div class="hidden sm:block" aria-hidden="true">
                <div class="py-5">
                    <div class="border-t border-gray-200"></div>
                </div>
            </div>

            <div class="mt-10 sm:mt-0">
                <div class="mt-5 md:col-span-2 md:mt-0">
                    <form action="{{ route('posts.update',[$post]) }}" method="POST"
                        class="w-full sm:px-6 lg:px-0 lg:col-span-9 mx-auto relative pb-20 space-y-6" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="overflow-hidden shadow sm:rounded-md">
                            <div class="bg-white px-4 py-5 sm:p-6">
                                <div class="grid grids-col-6 gap-2">
                                    <div class="col-span-6">
                                        <label for="title"
                                            class="block text-sm font-medium text-gray-700">{{__("Titre de l'article")}}</label>
                                        <input type="text" name="name" id="name" value="{{$post->name}}"
                                            class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                                    </div>
                                    
                                    <div class="col-span-6">
                                        <img src="/images/{{$post->image}}" alt="Image existante">
                                        <label for="image"
                                            class="block text-sm font-medium text-gray-700">{{__('Image de couverture')}}</label>
                                        <input type="file" accept="image/*" name="image" id="image"
                                            class="">
                                    </div>
                                    <div class="col-span-6">
                                        <label for="content" class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2">{{__('Contenue:')}}</label>
                                        <textarea name="content"  id="ckeditor-text">
                                            {!! $post->content !!}
                                        </textarea>
                                    </div>
                                    <div class="col-span-2">
                                        <label for="category"
                                        class="block text-sm font-medium text-gray-700">{{__('Catégorie')}}</label>
                                        <input type="text" name="category" id="category" value="{{$post->category->name}}"
                                            class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                                        <p>{{__("Catégories:")}}</p>
                                        <div class="grid grid-cols-3 gap-1">
                                            @forelse ($categories as $category)
                                                <p class="p-1 border border-black text-center">{{$category->name}}</p>
                                            @empty
                                                <p class='col-span-3'>{{__('Aucune catégorie existante')}}</p>
                                            @endforelse
                                        </div>
                                    </div>
                                    <div class="col-span-2">
                                        @php 
                                            $stringTag = $post->tags->map(function($tag){
                                                return $tag->name;
                                            });
                                        @endphp 
                                        <label for="tags"
                                            class="block text-sm font-medium text-gray-700">{{__('Tags')}}</label>
                                        <input type="text" name="tags" id="tags" value="{{implode(';',$stringTag->toArray())}}"
                                            class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                                        <p>Tags:</p>
                                        <div class="grid grid-cols-3 gap-1">
                                            @forelse ($tags as $tag)
                                                <p class="p-1 border border-black text-center">{{$tag->name}}</p>
                                            @empty
                                                <p class='col-span-3'>{{__('Aucun tag existante')}}</p>
                                            @endforelse
                                        </div>
                                    </div>
                                    <div class="col-span-2">
                                        <label for="published_at"
                                            class="block text-sm font-medium text-gray-700">{{__('Date publication')}}</label>
                                        <input type="date" name="published_at" id="published_at" value="{{$post->published_at->format('Y-m-d')}}"
                                            class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm">
                                    </div>
                                </div>
                            </div>
                            @can('post-create')
                            <div class="bg-gray-50 px-4 py-3 text-right sm:px-6">
                                <button type="submit"
                                    class="inline-flex justify-center rounded-md border border-transparent bg-indigo-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">Save</button>
                            </div>
                            @endcan
                        </div>
                    </form>
                </div>
            </div>

            <div class="hidden sm:block" aria-hidden="true">
                <div class="py-5">
                    <div class="border-t border-gray-200"></div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
<script src="https://cdn.ckeditor.com/ckeditor5/35.1.0/super-build/translations/fr.js"></script>

<script>
    // This sample still does not showcase all CKEditor 5 features (!)
    // Visit https://ckeditor.com/docs/ckeditor5/latest/features/index.html to browse all the features.
    CKEDITOR.ClassicEditor.create(document.getElementById("ckeditor-text"), {
        // https://ckeditor.com/docs/ckeditor5/latest/features/toolbar/toolbar.html#extended-toolbar-configuration-format
        toolbar: {
            items: [
                'findAndReplace', 'selectAll', '|',
                'heading', '|',
                'bold', 'italic', 'strikethrough', 'underline', 'removeFormat', '|',
                'bulletedList', 'numberedList', 'todoList', '|',
                'outdent', 'indent', '|',
                'undo', 'redo',
                '-',
                'fontSize', 'fontFamily', 'fontColor', 'fontBackgroundColor', 'highlight', '|',
                'alignment', '|',
                'link', 'blockQuote', 'insertTable', 'codeBlock', '|',
                'specialCharacters', 'horizontalLine','|',
                'sourceEditing'
            ],
            shouldNotGroupWhenFull: true
        },
        // Changing the language of the interface requires loading the language file using the <script> tag.
        language: 'fr',
        list: {
            properties: {
                styles: true,
                startIndex: true,
                reversed: true
            }
        },
        // https://ckeditor.com/docs/ckeditor5/latest/features/headings.html#configuration
        heading: {
            options: [
                { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
                { model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' },
                { model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4' },
                { model: 'heading5', view: 'h5', title: 'Heading 5', class: 'ck-heading_heading5' },
                { model: 'heading6', view: 'h6', title: 'Heading 6', class: 'ck-heading_heading6' }
            ]
        },
        // https://ckeditor.com/docs/ckeditor5/latest/features/editor-placeholder.html#using-the-editor-configuration
        placeholder: 'Contenue',
        // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-family-feature
        fontFamily: {
            options: [
                'default',
                'Arial, Helvetica, sans-serif',
                'Courier New, Courier, monospace',
                'Georgia, serif',
                'Lucida Sans Unicode, Lucida Grande, sans-serif',
                'Tahoma, Geneva, sans-serif',
                'Times New Roman, Times, serif',
                'Trebuchet MS, Helvetica, sans-serif',
                'Verdana, Geneva, sans-serif'
            ],
            supportAllValues: true
        },
        // https://ckeditor.com/docs/ckeditor5/latest/features/font.html#configuring-the-font-size-feature
        fontSize: {
            options: [ 10, 12, 14, 'default', 18, 20, 22 ],
            supportAllValues: true
        },
        // https://ckeditor.com/docs/ckeditor5/latest/features/link.html#custom-link-attributes-decorators
        link: {
            decorators: {
                addTargetToExternalLinks: true,
                defaultProtocol: 'https://',
                toggleDownloadable: {
                    mode: 'manual',
                    label: 'Downloadable',
                    attributes: {
                        download: 'file'
                    }
                }
            }
        },
        // The "super-build" contains more premium features that require additional configuration, disable them below.
        // Do not turn them on unless you read the documentation and know how to configure them and setup the editor.
        removePlugins: [
            // These two are commercial, but you can try them out without registering to a trial.
            'ExportPdf',
            'ExportWord',
            'CKBox',
            'CKFinder',
            'EasyImage',
            // This sample uses the Base64UploadAdapter to handle image uploads as it requires no configuration.
            // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/base64-upload-adapter.html
            // Storing images as Base64 is usually a very bad idea.
            // Replace it on production website with other solutions:
            // https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/image-upload.html
            'Base64UploadAdapter',
            'RealTimeCollaborativeComments',
            'RealTimeCollaborativeTrackChanges',
            'RealTimeCollaborativeRevisionHistory',
            'PresenceList',
            'Comments',
            'TrackChanges',
            'TrackChangesData',
            'RevisionHistory',
            'Pagination',
            'WProofreader',
            // Careful, with the Mathtype plugin CKEditor will not load when loading this sample
            // from a local file system (file://) - load this site via HTTP server if you enable MathType
            'MathType'
        ]
    });
</script>
