<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Models\Discussion;

class LastTopics extends Component
{
    private $discussions;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->discussions = Discussion::latest()->limit(5)->get();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.last-topics',['discussions' => $this->discussions]);
    }
}
