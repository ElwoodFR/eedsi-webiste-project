<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{

    protected $casts = [
        'date_debut' => 'datetime',
        'date_fin' => 'datetime',
    ];

    use HasFactory;

    protected $fillable = [
        'titre',
        'description',
        'image',
        'date_debut',
        'date_fin',
        'sponsor'
    ];
}
