<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{

    use SoftDeletes;
    use HasFactory;

    protected $casts = [
        'published_at' => 'datetime'
    ];

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'slug',
        'image',
        'content',
        'category_id',
        'published_at',
        'deleted_at'
    ];

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function tags(){
        return $this->belongsToMany(Tag::class);
    }

    public function scopePublished($query)
    {
      return $query->where('published_at', '<=', now());
    }
}
