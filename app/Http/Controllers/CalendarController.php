<?php

namespace App\Http\Controllers;

use App\Models\Calendar;
use Illuminate\Http\Request;

class CalendarController extends Controller
{
    public function index() {

        $calendars = Calendar::all();

        return view('calendars.index', compact('calendars'));
    }

    public function create() {
        return view('calendars.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'titre'     => 'required',
            'description'    => 'required',
            'date_debut'    => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'date_fin'    => '',
            'sponsor'    => '',
        ], [
            'titre.required' => 'Un titre est requis',
            'description.required' => 'Une description est requise',
            'image.required' => 'Une image est requise',
            'date_debut.required' => 'Une date de début est requise',
        ]);

        $input = $request->all();
  
        if ($image = $request->file('image')) {
            $destinationPath = 'images/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
        }
    
        Calendar::create($input);
    
   
        return redirect()->route('calendars.index')->with('success', 'Evenement créé avec succès');
    }

    /**
     * Display the specified resource.
     *
     * @param  Calendar  $calendar
     * @return Response
     */
    public function show(Calendar $calendar)
    {
        return view('calendars.show', compact('calendar'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Calendar  $calendar
     * @return Response
     */
    public function edit(Calendar $calendar)
    {
        return view('calendars.edit', compact('calendar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Calendar  $calendar
     * @return Response
     */
    public function update(Request $request, Calendar $calendar)
    {
        $this->validate($request, [
            'titre'     => 'required',
            'description'    => 'required',
            'date_debut'    => 'required',
            'date_fin'    => 'required',
            'sponsor'    => '',
        ], [
            'titre.required' => 'Un titre est requis',
            'description.required' => 'Une description est requise',
            'image.required' => 'Une image est requise',
            'date_debut.required' => 'Une date de début est requise',
            'date_fin.required' => 'Une date de fin est requise',
        ]);

        $input = $request->all();
  
        if ($image = $request->file('image')) {
            $destinationPath = 'images/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
        }else{
            unset($input['image']);
        }
          
        $calendar->update($input);

        return redirect()->route('calendars.index')->with('success', 'Evenement mis à jour avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Calendar  $calendar
     * @return Response
     */
    public function destroy(Calendar $calendar)
    {
        $calendar->delete();

        return redirect()->route('calendars.index')->with('success', 'Evenement supprimé avec succès');
    }
}
