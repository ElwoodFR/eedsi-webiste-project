<?php

namespace App\Http\Controllers;

use App\Models\Forum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ForumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $forums = Forum::all();
    
        return view('forums.index', compact('forums'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('forums.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'titre' =>  'required',
            'contenu'  =>  'required',
            'category_id' => 'required',
            'tags' => 'required'
        ]);

        $forum = Forum::create([
            'title' => $request->title,
            'content' => $request->content,
            'category_id' => $request->category_id,
            // 'slug' => str_slug($request->title)
            'user_id' => Auth::id()
        ]);

        $forum->tags()->attach($request->tags);

        Session::flash('success', 'Post created succesfully.');


        return redirect()->back();
    }
}
