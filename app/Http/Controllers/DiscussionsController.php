<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Discussion;
use App\Models\Channel;
use App\Models\Reply;

class DiscussionsController extends Controller
{
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $channels = Channel::all();
        return view('discussion.create',compact('channels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => "required",
            'channel_id' => 'required',
            'content' => 'required'
        ],[
            'title' => __('Un titre est obligatoire'),
            'channel_id' => __('Veuillez sélectionner une catégorie'),
            "content" => __('Un minimum de contenue doit être présent')
        ]);
        $newDiscussion = Discussion::create([
            'title' => $request->title,
            "channel_id" => $request->channel_id,
            "user_id" => Auth()->user()->id,
            'content' => $request->content
        ]);
        return redirect()->route('discussion.show',[$newDiscussion]);
    }

    /**
     * Display the specified resource.
     *
     * @param  Discussion $discussion
     * @return \Illuminate\Http\Response
     */
    public function show(Discussion $discussion)
    {
        return view("discussion.show",compact("discussion"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
