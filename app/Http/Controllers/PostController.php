<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::paginate(30);
        $categories = Category::all();
        $tags = Tag::all();

        return view('posts.index', compact('posts'))->with('tags', $tags)->with('categories', $categories);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('posts.create')->with('categories', $categories)->with('tags', $tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'          => 'required',
            'image'         => 'required',
            'content'       => 'required',
            'published_at'  => 'required',
            
        ],[
            'name.required' => 'Le nom de l\'article est requis.',
            'image.required' => 'L\'image de l\'article est requise',
            'content.required' => 'Le contenu de l\'article est requise',
            'published_at.required' => 'La date de publication de l\'article est requise',

        ]);


        //Gestion des tags
        $tagsListe = collect(explode(';',$request->tags));
        $tagsIdList = $tagsListe->map(function($tag){
            return Tag::firstOrCreate(['name' => $tag ],['color' => "#D1D1D1"])->id;
        });

        $category = Category::firstOrCreate(['name' => $request->category],['color' => "#D1D1D1"]);
        $couverture = "";
        if ($image = $request->file('image')) {
            $destinationPath = 'images/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $couverture = "$profileImage";
        }

        
        $post = Post::create([
            'name' => $request->name,
            'slug' => Str::slug($request->name),
            'image' => $couverture,
            'content' => $request->content,
            'category_id' => $category->id,
            'published_at' => $request->published_at,
        ]);

        // Post Tag mapping
        $post->tags()->sync($tagsIdList);
        
   
        return redirect()->route('posts.index')->with('success', 'Article créé avec succès');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('posts.edit')->with('post', $post)
                                      ->with('categories', Category::all())
                                      ->with('tags', Tag::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $this->validate($request, [
            'name'          => 'required',
            'content'       => 'required',
            'published_at'  => 'required',
            
        ],[
            'name.required' => 'Le nom de l\'article est requis.',
            'content.required' => 'Le contenu de l\'article est requise',
            'published_at.required' => 'La date de publication de l\'article est requise',
        ]);
        $imageCurrent = $post->image;
        if ($image = $request->file('image')) {
            $destinationPath = 'images/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $imageCurrent= "$profileImage";
        }

        $tagsListe = collect(explode(';',$request->tags));
        $tagsIdList = $tagsListe->map(function($tag){
            return Tag::firstOrCreate(['name' => $tag ],['color' => "#D1D1D1"])->id;
        });

        $category = Category::firstOrCreate(['name' => $request->category],['color' => "#D1D1D1"]);

        $post->update([
            'name' => $request->name,
            'slug' => Str::slug($request->name),
            'image' => $imageCurrent,
            'content' => $request->content,
            'category_id' => $category->id,
            'published_at' => $request->published_at,
        ]);

        $post->tags()->sync($tagsIdList);

        return redirect()->route('posts.index')->with('success', 'Artcile mis à jour avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
      	$post->published_at = NULL;
      	$post->save();
        $post->delete();

        return redirect()->route('posts.index')->with('success', 'Article placé dans la corbeille.');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function trashed(Post $post) {

        $posts = Post::onlyTrashed()->get();
        
        return view('posts.trashed', compact('posts'));

    }

    public function restore($id)
    {
        $post = Post::withTrashed()->where('id', $id)->first();

        $post->restore();

        return redirect()->route('posts.index')->with('success', 'Article bien restauré.');
    }

    public function kill($id)
    {
        $post = Post::withTrashed()->where('id', $id)->first();

        $tag = Tag::all();

        $post->forceDelete();
        $post->tags()->detach($tag);
        

        return redirect()->route('posts.trashed')->with('success', 'Adieu pauvre petit article, tu es parti trop tôt!');

    }
}
