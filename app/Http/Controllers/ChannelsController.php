<?php

namespace App\Http\Controllers;

use App\Models\Channel;
use App\Models\Discussion;
use Illuminate\Http\Request;

class ChannelsController extends Controller
{

    /**
     * Display all existing channels
     * @return Response
     */
    public function index(){
        $channels = Channel::all();
        return view('channels.index',compact('channels'));
    }

    /**
     * Display the form used to create a new channel
     * @return Response
     */
    public function create(){
        return view("channels.create");
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request $request
     * @return Response
     */
    public function store(Request $request){
        $this->validate($request,[
            'title' => 'required'
        ],[
            'title' => __('Un titre est requis')
        ]);

        Channel::create([
            'title' => $request->title
        ]);
        
        session()->flash('succes',__('Catégorie créée'));
        return redirect()->route('channels.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  Channel  $channel
     * @return Response
     */
    public function show(Channel $channel){
        $channel->load(['discussions']);

        return view('channels.show',compact("channel"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Channel  $channel
     * @return Response
     */
    public function edit(Channel $channel){
        return view('channels.edit',compact('channel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Channel  $channel
     * @return Response
     */
    public function update(Request $request, Channel $channel){
        $this->validate($request,[
            'title' => 'required',
        ],[
            'title' => __('Un titre est requis')
        ]);
        $channel->title = $request->title; 
        $channel->save();

        return redirect()->route('channels.index')->with('success',"Catégorie {$channel->title} mis à jour");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Channel  $channel
     * @return Response
     */
    public function destroy(Channel $channel)
    {
        $channel->delete();

        return redirect()->route('channels.index')->with('success', 'Catégorie supprimé avec succès');
    }
}
