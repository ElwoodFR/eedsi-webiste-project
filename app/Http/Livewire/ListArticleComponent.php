<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Post;

class ListArticleComponent extends Component
{
    use WithPagination;
    public function render()
    {
        return view('livewire.list-article-component',[
            'posts' => Post::with('category')->paginate(10),
        ]);
    }
}
