<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Reply;

class RepliesList extends Component
{
    use WithPagination;

    public $discussion;
    public $newContent;
    

    public function render()
    {
        $replies = Reply::where('discussion_id',$this->discussion)->paginate(10);

        return view('livewire.replies-list',compact('replies'));
    }

    //Enregistrement des nouvelles réponses
    public function saveReply(){
        Reply::create([
            'content' => $this->newContent,
            'discussion_id' => $this->discussion,
            'user_id' => auth()->user()->id
        ]);
        $this->emit('clean-editor');
        
    }
}
