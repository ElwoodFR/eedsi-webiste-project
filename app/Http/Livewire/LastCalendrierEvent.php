<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Calendar;

class LastCalendrierEvent extends Component
{
    public function render()
    {
        $calendar = Calendar::latest()->first();
        return view('livewire.last-calendrier-event',compact('calendar'));
    }
}
