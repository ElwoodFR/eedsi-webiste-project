<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Discussion;

class DiscussionsList extends Component
{
    use WithPagination;
    public function render()
    {
        return view('livewire.discussions-list',[
            "discussions" => Discussion::paginate(10),
        ]);
    }
}
