<?php
  
namespace Database\Seeders;
  
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
  
class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
            'permission-list',
            'permission-create',
            'permission-edit',
            'permission-delete',
            'posts-list',
            'post-create',
            'post-edit',
            'post-delete',
            'event-list',
            'event-create',
            'event-edit',
            'event-delete',
            'topic-list',
            'topic-create',
            'topic-edit',
            'topic-delete',
            'channel-list',
            'channel-edit',
            'channel-destroy',
            'discussion-create',
            'discussion-delete'
        ];
     
        foreach ($permissions as $permission) {
             Permission::create(['name' => $permission]);
        }
    }
}