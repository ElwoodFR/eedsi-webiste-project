<?php
  
namespace Database\Seeders;
  
use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
  
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name' => 'Administrateur', 
            'email' => 'admin@admin.com',
            'password' => bcrypt('password')
        ]);
    
        $role = Role::create(['name' => 'admin']);
     
        $permissions = Permission::pluck('id','id')->all();
   
        $role->syncPermissions($permissions);
     
        $admin->assignRole([$role->id]);

        $user = User::create([
            'name' => 'Utilisateur', 
            'email' => 'user@user.com',
            'password' => bcrypt('password')
        ]);
    
        $role_user = Role::create(['name' => 'user']);

        $role_user->givePermissionTo('topic-list');
        $role_user->givePermissionTo('topic-create');
        $role_user->givePermissionTo('topic-edit');
        $role_user->givePermissionTo('topic-delete');
     
        $user->assignRole([$role_user->id]);
    }
}
