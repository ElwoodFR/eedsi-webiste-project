<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Project

**FR :**

Ceci est notre dernier projet d'étude au sein d'ApFormation. Il s'agit d'un site web qui référence les tournois de cartes de la Région, il y a aussi une partie Forum pour aider les joueurs à répondre à certaines interrogations, une partie blog et une partie calendrier qui référence la liste des tournois

Fonctionnalités :

- Authentification (inscription, connexion)
- Blog (création, édition, suppression, lecture d'un ou plusieurs posts)
- Forum (création, édition, suppression, lecture d'un ou plusieurs topics)
- Calendrier (création, édition, suppression, lecture de date de tournois de carte)


**EN :**

This is our last study project within ApFormation. It is a website that references the card tournaments of the region, there is also a forum part to help players to answer some questions, a blog part and a calendar part that references the list of tournaments

Features :

- Authentication (registration, connection)
- Blog (creation, edition, deletion, reading of one or more posts)
- Forum (creation, edition, deletion, reading of one or more topics)
- Calendar (creation, edition, deletion, reading of card tournament dates)


