<?php

use App\Http\Controllers\CalendarController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ChannelsController;
use App\Http\Controllers\DiscussionsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

//---------- NO REGISTER VIEWS ----------//

Route::get('calendars', [CalendarController::class, 'index'])->name('calendars.index');

Route::get('posts', [PostController::class, 'index'])->name('posts.index');
Route::get('posts/show/{post}', [PostController::class, 'show'])->name('posts.show');

Route::get('channels', [ChannelsController::class, 'index'])->name('channels.index');
Route::get('channels/show', [ChannelsController::class, 'show'])->name('channels.show');
Route::get('/discussion/show/{discussion}', [DiscussionsController::class, 'show'])->name('discussion.show');

//---------- REGISTER VIEWS ----------//

Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::resource('permissions', PermissionController::class);

    // CALENDAR
    Route::get('calendars/create', [CalendarController::class, 'create'])->name('calendars.create');
    Route::post('calendars/store', [CalendarController::class, 'store'])->name('calendars.store');
    Route::get('calendars/{calendar}/edit', [CalendarController::class, 'edit'])->name('calendars.edit');
    Route::put('calendars/update', [CalendarController::class, 'update'])->name('calendars.update');
    Route::delete('calendars/{calendar}/destroy', [CalendarController::class, 'destroy'])->name('calendars.destroy');

    // POST
    Route::get('posts/create', [PostController::class, 'create'])->name('posts.create');
    Route::post('posts/store', [PostController::class, 'store'])->name('posts.store');
    Route::get('posts/{post}/edit', [PostController::class, 'edit'])->name('posts.edit');
    Route::put('posts/update/{post}', [PostController::class, 'update'])->name('posts.update');
    Route::delete('posts/{post}/destroy', [PostController::class, 'destroy'])->name('posts.destroy');

    // FORUM
    Route::get('channels/create', [ChannelsController::class, 'create'])->name('channels.create');
    Route::post('channels/store', [ChannelsController::class, 'store'])->name('channels.store');
    Route::get('channels/{channel}/edit', [ChannelsController::class, 'edit'])->name('channels.edit');
    Route::put('channels/update', [ChannelsController::class, 'update'])->name('channels.update');
    Route::delete('channels/{channel}/destroy', [ChannelsController::class, 'destroy'])->name('channels.destroy');

    // Route::resource('channels',ChannelsController::class);

    Route::get('discussion/create/new', [DiscussionsController::class, 'create'])->name('discussions.create');

    Route::post('discussions/store', [DiscussionsController::class, 'store'])->name('discussions.store');
    
    Route::post('/discussion/reply/{id}', [DiscussionsController::class, 'reply'])->name('discussions.reply');
    
    // Route::get('/reply/like/{id}', [RepliesController::class, 'like'])->name('reply.like');
    
    // Route::get('/reply/unlike/{id}', [RepliesController::class, 'unlike'])->name('reply.unlike');

    // Route::get('/discussion/watch/{id}', [WatchersController::class, 'watch'])->name('discussion.watch');

    // Route::get('/discussion/unwatch/{id}', [WatchersController::class, 'unwatch'])->name('discussion.unwatch');

    // Route::get('/discussion/best/reply/{id}', [RepliesController::class, 'best_answer'])->name('discussion.best.answer');

});

require __DIR__.'/auth.php';
